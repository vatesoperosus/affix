# affix
## WIP: this project is still very ugly and not user-friendly.

affix is a customizable  [Übersicht](https://github.com/felixhageloh/uebersicht) widget
written in React JSX, Tailwindcss, and DaisyUI created for macOS + tiling window managers.

![Desktop](./screenshots/1.png)

## Dependencies
 - [Übersicht](https://github.com/felixhageloh/uebersicht)
 - [pywal](https://github.com/dylanaraps/pywal) (now optional)
 - [Hack Nerd Font](https://github.com/ryanoasis/nerd-fonts)(workspace bubbles and generally good to have)
 - NPM/yarn

## Recommended
 - [yabai](https://github.com/koekeishiya/yabai)
 - [skhd](https://github.com/koekeishiya/skhd)
 - [hyper](https://github.com/vercel/hyper)
 - [pywal](https://github.com/dylanaraps/pywal)(Requires a Tailwindcss compile)

## Components 

- Yabai workspaces
- Date/Time
- Currently playing music from:
  - Spotify
- Wifi
- Battery
- Volume

## Installation

- Clone this project into your widgets folder
- Navigate into the src directory and run `yarn` or `npm install` to pull dependencies
- Adjust filepaths in index.jsx and components/styles.jsx to match your corresponding directories
- Airport will need to be in your PATH. The easiest way to do that is 
```
sudo ln -s /System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport /usr/local/bin/airport
```
- You may need to make the scripts executable `chmod +x /path/to/scripts.sh`


