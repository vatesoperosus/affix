import { run, React } from "uebersicht";
import useRefresh from "./src/hooks/refresh";
import {
  CurrentlyPlaying,
  Workspaces,
  Time,
  Volume,
  Wifi,
  Battery,
  Weather,
} from "./src/components/index.jsx";

// inject DaisyUI theme selection
const root = document.getElementById("uebersicht");
root.setAttribute("data-theme", "Kanagawa");

export const refreshFrequency = false;

export const render = () => {
  const menuBarHeight = run("affix/scripts/menu-height.swift");
  let content = (
    <div className={`main-bar text-xs h-${menuBarHeight}`}>
      <link
        rel="stylesheet"
        type="text/css"
        href="affix/assets/tabler-icons.css"
      />
      <link
        rel="stylesheet"
        type="text/css"
        href="affix/src/css/tailwind.css"
      />
      <div className="left-bar">
        <Weather />
      </div>
      <div className="center-bar" />
      <div className="right-bar">
        <CurrentlyPlaying />
        <Wifi />
        <Volume />
        <Battery />
        <Time />
      </div>
    </div>
  );
  return content;
};
