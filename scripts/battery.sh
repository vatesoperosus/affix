#!/bin/bash

percentage=`pmset -g batt | grep -Eo "\d+%"`
chargingState=`pmset -g batt | grep -Eo '(charging|discharging)'`
remaining=`pmset -g batt | grep -Eo "[0-9]?[0-9]:[0-9][0-9]*"`

JSON_STRING='{"percentage":"'"$percentage"'","chargingState":"'"$chargingState"'", "remaining":"'"$remaining"'"}'
echo ${JSON_STRING}
