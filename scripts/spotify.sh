#!/bin/bash

is_running="$(pgrep Spotify)"

if [ -n "$is_running" ]
then
  artist="$(echo `osascript -e 'tell application "Spotify" to artist of current track as string'`)" 
  track="$(echo `osascript -e 'tell application "Spotify" to name of current track as string'`)" 
  echo -e "$artist - $track"
fi
