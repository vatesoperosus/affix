import { run, React } from "uebersicht";
import useRefresh from "../hooks/refresh";

export const Battery = () => {
  const [state, setState] = React.useState({
    percentage: "",
    chargingState: "",
    remaining: "",
  });

  const [showMenu, setShowMenu] = React.useState(false);
  const [caffeinated, setCaffeinated] = React.useState("");

  const getBattery = async () => {
    const [battery, caffeinated] = await Promise.all([
      run("affix/scripts/battery.sh"),
      run("pgrep caffeinate"),
    ]);
    setState(JSON.parse(battery));
    setCaffeinated(caffeinated);
  };
  useRefresh(getBattery, 10000, true);

  const toggleCaffeinate = () => {
    !caffeinated.length ? run("caffeinate -d &") : run("pkill -f caffeinate");
    getBattery();
  };

  const toggleShowMenu = () => {
    setShowMenu(!showMenu);
  };

  const handleClick = (e) => {
    const { ctrlKey } = e.nativeEvent;
    ctrlKey ? toggleCaffeinate() : toggleShowMenu();
  };

  const { percentage, chargingState, remaining } = state;

  var iconName = (percentage, chargingState) => {
    var percentage = parseInt(percentage);
    if (chargingState === "charged") return "ti-battery-charging text-success";
    if (chargingState === "charging") return "ti-battery-charging text-success";
    if (percentage > 90) return "ti-battery-4 text-success";
    if (percentage > 60) return "ti-battery-3 text-info";
    if (percentage > 30) return "ti-battery-2 text-warning";
    if (percentage > 10) return "ti-battery-1 text-error";
    return "ti-battery text-error";
  };

  return (
    <div
      className={`element hoverable ${caffeinated && "bg-success/20"} ${
        showMenu && "w-[5vw]"
      }`}
      onClick={handleClick}
    >
      <i className={"icon ti " + iconName(percentage, chargingState)} />
      {showMenu ? <span>{remaining}</span> : <span>{percentage}</span>}
    </div>
  );
};
