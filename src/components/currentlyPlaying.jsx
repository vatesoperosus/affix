import { run, React } from "uebersicht";
import useRefresh from "../hooks/refresh";

export const CurrentlyPlaying = () => {
  const [state, setState] = React.useState({ currentlyPlaying: "" });

  const getCurrentlyPlaying = () => {
    run("affix/scripts/spotify.sh").then((spotify) => {
      setState({ currentlyPlaying: spotify });
    });
  };

  useRefresh(getCurrentlyPlaying, 3000, true);

  const playPause = () => {
    run(`osascript -e 'tell application "Spotify" to playpause'`);
  };

  const { currentlyPlaying } = state;
  return (
    <div
      className="element right-notch max-w-[16vw] mr-auto"
      onClick={playPause}
    >
      <span>
        <i className="icon text-success ti ti-brand-spotify" />
      </span>
      <div className="long">{currentlyPlaying}</div>
    </div>
  );
};
