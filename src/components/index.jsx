export { Battery } from "./battery.jsx";
export { Workspaces } from "./workspaces.jsx";
export { Time } from "./time.jsx";
export { Volume } from "./volume.jsx";
export { Wifi } from "./wifi.jsx";
export { Weather } from "./weather.jsx";
export { CurrentlyPlaying } from "./currentlyPlaying.jsx"
