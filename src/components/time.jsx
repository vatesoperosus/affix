import { React } from 'uebersicht'
import useRefresh from '../hooks/refresh'

export const Time = () => {
  const [time, setTime] = React.useState()

  var dateOptions = {
    weekday: "long",
    month: "short",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
    hour12: false
  };
  const getTime = () => {

    let d = new Date()
    const date = d.toLocaleString("en-US", dateOptions)
    setTime(date)
  }

  useRefresh(getTime, 1000, true)

  return (
    <div className='element text-center hoverable'>
      <b>{time}</b>
    </div>
  );
};

