import { run, React } from "uebersicht";
import useRefresh from "../hooks/refresh";

export const Volume = () => {
  const [state, setState] = React.useState({ volume: null, muted: null });
  const { volume: _volume, muted: _muted } = state || {};
  const [showMenu, setShowMenu] = React.useState(false);
  const [volume, setVolume] = React.useState(_volume && _volume);
  const [isSliding, setIsSliding] = React.useState(false);
  const [mute, setMute] = React.useState(true);

  React.useEffect(() => {
    if (!isSliding) dispatchVolume(volume);
  }, [isSliding]);

  React.useEffect(() => {
    if (_muted != mute) dispatchMute(mute)
  }, [mute])

  React.useEffect(() => {
    if (_muted && _muted !== mute) {
      setMute(_muted);
    }
    if (_volume && _volume !== volume) {
      setVolume(_volume);
    }
  }, [_volume, _muted]);

  const dispatchMute = (targetMute) => {
    run(`osascript -e 'set volume output muted ${targetMute}'`);
  };

  const dispatchVolume = (targetVolume) => {
    if (!targetVolume) return;
    run(`osascript -e 'set volume output volume ${targetVolume}'`);
  };

  const getVolume = () => {
    run(`osascript -e 'get volume settings'`).then((settings) => {
      var { 0: output, 3: muted } = settings.split(",");
      setState({
        volume: parseInt(output.split(":")[1]),
        muted: muted.split(":")[1].trim() === "true",
      });
    });
  };

  useRefresh(getVolume, 10000, true);

  const toggleMute = (e) => {
    setMute(!mute);
  };

  const toggleShowMenu = () => {
    setShowMenu(!showMenu);
  };

  const handleClick = (e) => {
    const { ctrlKey } = e.nativeEvent;
    if (ctrlKey === false) {
      toggleShowMenu();
    }
    if (ctrlKey === true) {
      toggleMute();
    }
  };

  const handleSlider = (e) => {
    setVolume(parseInt(e.target.value));
  };

  const onMouseDown = () => setIsSliding(true);
  const onMouseUp = () => setIsSliding(false);

  var iconName = () => {
    const { volume, muted } = state;

    if (mute === true) return "ti-volume-3 text-accent";
    if (volume > 50) return "ti-volume text-primary";
    if (volume > 0) return "ti-volume-2 text-secondary";
    if (volume == 0) return "-volume-off";
    return "ti-volume-off";
  };

  return (
    <>
      <div className={`element hoverable ${showMenu && "w-56"} `}>
        <i
          className={`icon ti mr-0 text-[18px] ${showMenu && "mr-1"
            } ${iconName()}`}
          onClick={handleClick}
        />
        {showMenu && (
          <input
            type="range"
            className="slider"
            min="0"
            max="100"
            step="1"
            value={volume}
            onChange={handleSlider}
            onMouseDown={onMouseDown}
            onMouseUp={onMouseUp}
          />
        )}
      </div>
    </>
  );
};
