import { run, React } from "uebersicht";
import useRefresh from "../hooks/refresh";

export const Weather = () => {
  const [state, setState] = React.useState();
  const [error, setError] = React.useState();

  const getWeather = async () => {
    run(`curl -s 'v2n.wttr.in?m&format="%t+%C+%c'`)
      .then((res) => {
        const cleaned = res.replace(/\"/, "");
        if (cleaned.includes("Sorry")) {
          setError(true);
        } else {
          setState(cleaned);
        }
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useRefresh(getWeather, 900000, true);

  return (
    <div className="element hoverable ml-auto mr-1">{!error && state}</div>
  );
};
