import { run, React } from "uebersicht";
import useRefresh from "../hooks/refresh";

export const Wifi = () => {
  const [state, setState] = React.useState({ ssid: "", rssi: "" });
  const [ip, setIp] = React.useState();
  const [showMenu, setShowMenu] = React.useState(false);
  const [showSsid, setShowSsid] = React.useState(false);

  const getWifi = () => {
    run(`airport -I`).then((wifi) => {
      var results = wifi.split(/\n/);
      var ssid = results[12].split(":")[1];
      var rssi = results[0].split(":")[1];
      setState({ ssid: ssid, rssi: parseInt(rssi) });
    });
    run(`ipconfig getifaddr en0`).then((ip) => {
      setIp(ip);
    });
  };

  useRefresh(getWifi, 10000, true);

  const toggleShowMenu = () => {
    setShowMenu(!showMenu);
  };

  const handleClick = (e) => {
    const { ctrlKey } = e.nativeEvent;
    ctrlKey ? null : toggleShowMenu();
  };

  const getIcon = () => {
    const { rssi } = state;
    if (rssi > -55) return "ti-wifi text-info";
    if (rssi > -70) return "ti-wifi-2 text-info";
    if (rssi > -75) return "ti-wifi-1 text-warn";
    if (rssi > -80) return "ti-wifi-0 text-error";
  };
  const { ssid } = state;
  return (
    <div
      className={`element hoverable ${showMenu && "w-[7vw]"}`}
      onClick={handleClick}
    >
      <i
        className={`icon ti mr-0 text-[18px] ${getIcon()} ${
          showMenu || (showSsid && "mr-1")
        }`}
      />
      {!showMenu && showSsid && <div>{ssid}</div>}
      {showMenu && <div>{ip}</div>}
    </div>
  );
};
