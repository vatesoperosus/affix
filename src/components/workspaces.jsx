import { run, React } from "uebersicht";
import useRefresh from "../hooks/refresh";

export const Workspaces = () => {
  const [workspaces, setWorkspaces] = React.useState();
  const [showMenu, toggleShowMenu] = React.useState(false);

  const getWorkspaces = async () => {
    run(`yabai -m query --spaces`).then((results) => {
      setWorkspaces(JSON.parse(results));
    });
  };
  useRefresh(getWorkspaces, 500, true);

  return (
    <span className="element workspaces">
      {workspaces &&
        workspaces.length > 1 &&
        workspaces.map(({ index, "has-focus": focused }) => (
          <span className={`workspace ${focused ? 'focused' : 'unfocused'}`} key={`workspace ${index}`}>
            {focused ? "" : ""}
          </span>
        ))}
    </span>
  );
};
