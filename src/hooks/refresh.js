import { React } from 'uebersicht'

const useRefresh = (get, frequency, init) => {

  React.useEffect(() => {
    if (init) {
      get()
      const interval = setInterval(get, frequency)
      return () => clearInterval(interval)

    }
  }, [init])
}

export default useRefresh
