/** @type {import('tailwindcss').Config} */
const Kanagawa = require('./assets/themes/kanagawa')
const Wal = require('./assets/themes/wal')
module.exports = {
  content: ["./src/components/*.{html,js,jsx}", "index.jsx"],
  theme: {
    extend: {
      transitionProperty: {
        width: "width",
      },
    },
  },
  fontFamily: {
    sans: ["SF Pro", "sans-serif"],
    serif: ["SF Pro Condensed", "serif"],
  },
  plugins: [require("daisyui")],
  daisyui: {
    base: false,
    themes: [
      {
        kanagawa: {
          primary: Kanagawa.colors.color13,
          secondary: Kanagawa.colors.color12,
          accent: Kanagawa.colors.color17,
          neutral: Kanagawa.special.background,
          'neutral-focused': Kanagawa.special.foreground,
          "base-100": Kanagawa.colors.color5,
          info: Kanagawa.colors.color12,
          success: Kanagawa.colors.color10,
          error: Kanagawa.colors.color9,
          warning: Kanagawa.colors.color16
        }
      },
      () => {
        if (Wal) return {
          wal: {
            primary: Wal.color3,
            secondary: Wal.color4,
            accent: Wal.color7,
            neutral: Wal.background,
            'neutral-focused': Wal.special.foreground,
            "base-100": Wal.colors.color5,
            info: Wal.colors.color12,
            success: Wal.colors.color10,
            error: Wal.colors.color9,
            warning: Wal.colors.color16
          }

        }
      }
    ],
    darkTheme: 'Kanagawa'

  },
};
